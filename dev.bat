if not exist venv (
  ECHO "INIT VENV"
  CALL python -m venv venv || GOTO :exit
)
CALL venv\Scripts\activate || GOTO :exit

CALL python -m pip install --upgrade pip==22.0.1 pip-tools==6.6.0 doit==0.36.0 || GOTO :exit

CALL python -m pip install --upgrade -r requirements.txt || GOTO :exit

CALL python manage.py makemigrations --noinput || GOTO :exit
CALL python manage.py migrate --noinput || GOTO :exit