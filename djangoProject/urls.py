from django.contrib import admin
from django.urls import path
from django.conf.urls import include

versions = [
    path('v1/', include('dish_restaurant.api.urls', namespace='dish_v1')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include((versions, 'dish_restaurant'), namespace='dish_api')),
]
