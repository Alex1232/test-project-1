from django.contrib import admin

from dish_restaurant.models import (FoodCategory, Food)

admin.site.register(FoodCategory)
admin.site.register(Food)
