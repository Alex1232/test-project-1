from django.db import models


class TimeStampedModel(models.Model):
    date = models.DateTimeField()
