from rest_framework import routers

from dish_restaurant.api.views import FoodView

app_name = 'dish_restaurant'

router = routers.DefaultRouter()
router.register(r'foods', FoodView, basename='foods')
urlpatterns = []
urlpatterns += router.urls
