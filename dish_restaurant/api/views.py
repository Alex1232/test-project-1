from rest_framework.viewsets import ModelViewSet

from dish_restaurant.api.serializers import FoodListSerializer
from dish_restaurant.models import FoodCategory


class FoodView(ModelViewSet):
    serializer_class = FoodListSerializer
    queryset = FoodCategory.objects

    def list(self, request, *args, **kwargs):
        serializer = super().list(request, *args, **kwargs)
        collect_category = []

        for category in serializer.data:
            collect_foods = []
            for food in category['foods']:
                if food['is_publish']:
                    collect_foods.append(food)
            if not category['foods'] or not collect_foods:
                collect_category.append(category)

            category['foods'] = collect_foods

        for category in collect_category:
            serializer.data.remove(category)

        return serializer
